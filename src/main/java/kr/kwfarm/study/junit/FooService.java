package kr.kwfarm.study.junit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class FooService {
    private Logger logger = LoggerFactory.getLogger(FooService.class);

    public String getString() {
        return "a";
    }

    public void exception() {
        throw new RuntimeException("Hello Exception");
    }

    public void thread(int sleepMill) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Future<String> task = executorService.submit(() -> {
            try {
                TimeUnit.MILLISECONDS.sleep(sleepMill);
            } catch (InterruptedException e) {
                logger.error(e.getMessage(), e);
            }
            return String.valueOf(sleepMill);
        });

        task.get();

    }
}
