package kr.kwfarm.stjdy.junit;

import kr.kwfarm.study.junit.FooService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class Test02Assertions {
    @Test
    void standardAssertions() {
        assertEquals(2, 2);
        assertEquals(4, 4, "The optional assertion message is now the last parameter.");
        assertTrue(2 == 2, () -> "Assertion messages can be lazily evaluated -- "
                + "to avoid constructing complex messages unnecessarily.");
    }

    @Test
    void groupedAssertions() {
        assertAll("hello", () -> assertEquals(1, 1), () -> assertEquals(2, 2));
    }

    @Test
    void exception() {
        FooService fooService = new FooService();

        RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> {
            fooService.exception();
        });
    }

    @Test
    void notExceptionThrow() {
        FooService fooService = new FooService();

        RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> {
            fooService.getString();
        });
    }

    @Test
    void timeout() {
        FooService fooService = new FooService();

        assertTimeout(Duration.ofMillis(200), () -> {fooService.thread(300);});
    }

    @Test
    void timeout_이내() {
        FooService fooService = new FooService();

        assertTimeout(Duration.ofMillis(200), () -> {fooService.thread(100);});
    }
}
