package kr.kwfarm.stjdy.junit;

import kr.kwfarm.stjdy.junit.parameters.TestType;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.JavaTimeConversionPattern;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;
import org.junit.jupiter.params.provider.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.time.LocalDate;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Test06Parameterized {
    private Logger logger = LoggerFactory.getLogger(Test06Parameterized.class);

    @ParameterizedTest
    @ValueSource(strings = {"abc", "bcd", "cde"})
    void test(String param) {
        logger.info("Param: {}", param);
        assertTrue(param.contains("b"));
    }

    @ParameterizedTest
    @EnumSource(value = TestType.class)
    void parameterEnumSource(TestType testType) {
        logger.info("parameterEnumSource: {}", testType);
    }

    @ParameterizedTest
    @EnumSource(value = TestType.class, names = {"TEST01", "TEST03"})
    void parameterEnumSourceChoose(TestType testType) {
        logger.info("parameterEnumSource: {}", testType);
        assertTrue(EnumSet.of(TestType.TEST01, TestType.TEST03).contains(testType));
    }

    @ParameterizedTest
    @EnumSource(value = TestType.class, names = {"TEST01", "TEST02"})
    void parameterEnumSourceChooseFail(TestType testType) {
        logger.info("parameterEnumSource: {}", testType);
        assertTrue(EnumSet.of(TestType.TEST01, TestType.TEST03).contains(testType));
    }

    @ParameterizedTest
    @MethodSource("rangs")
    void parameterMethod(int i) {
        logger.info("parameterMethod: {}", i);
    }

    @ParameterizedTest
    @ArgumentsSource(MyArgumentsProvider.class)
    void parameterArgumentSource(String argument) {
        logger.info("parameterArgumentSource: {}", argument);
    }

    @ParameterizedTest
    @ValueSource(strings = {"true", "false"})
    void convert(Boolean aBoolean) {
        logger.info("convert: {}", aBoolean);
    }

    @ParameterizedTest
    @EnumSource(TimeUnit.class)
    void customConverter(@ConvertWith(CustomConverter.class) String argument) {
        logger.info("customConverter: {}", argument);
    }

    @ParameterizedTest
    @ValueSource(strings = { "01.01.2017", "31.12.2017" })
    void testWithExplicitJavaTimeConverter(@JavaTimeConversionPattern("dd.MM.yyyy") LocalDate argument) {
        logger.info("testWithExplicitJavaTimeConverter: {}", argument);
    }

    static IntStream rangs() {
        return IntStream.range(0, 20).skip(10);
    }

    static class MyArgumentsProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of("foo", "bar").map(Arguments::of);
        }
    }

    static class CustomConverter extends SimpleArgumentConverter {

        @Override
        protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
            System.out.println(targetType.getName());
            return String.valueOf(source);
        }
    }
}
