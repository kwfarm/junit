package kr.kwfarm.stjdy.junit;

import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Test03Nested {
    private static Logger logger = LoggerFactory.getLogger(Test03Nested.class);

    public Test03Nested() {
        logger.info("=======[NestedTest]=======");
    }

    @BeforeAll
    static void beforeAll() {
        logger.info("==========[BeforeAll]==========");
    }

    @BeforeEach
    void beforeEach() {
        logger.info("=====[BeforeEach]=====");
    }

    @Test
    void main() {
        logger.info("main");
    }

    @Nested
    class Foo {
        public Foo() {
            logger.info("=======[Foo]=======");
        }

        @Test
        void foo01() {
            logger.info("Foo-Foo01");
        }

        @Test
        void foo02() {
            logger.info("Foo-Foo02");
        }
    }

    @Nested
    @DisplayName("Bar Test")
    class Bar {
        public Bar() {
            logger.info("=======[Bar]=======");
        }

        @Test
        void bar01() {
            logger.info("Bar-Bar01");
        }

        @Test
        void bar02() {
            logger.info("Bar-Bar02");
        }
    }
}
