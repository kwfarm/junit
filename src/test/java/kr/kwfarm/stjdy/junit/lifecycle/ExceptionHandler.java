package kr.kwfarm.stjdy.junit.lifecycle;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionHandler implements TestExecutionExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @Override
    public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        logger.info("Exception: {}", throwable.getClass().getName());
    }
}
