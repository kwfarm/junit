package kr.kwfarm.stjdy.junit.lifecycle;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecutionCallback implements BeforeTestExecutionCallback, AfterTestExecutionCallback {
    private Logger logger = LoggerFactory.getLogger(ExecutionCallback.class);

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception {
        logger.info("5. before Execution");
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        logger.info("6. after Execution");
    }
}
