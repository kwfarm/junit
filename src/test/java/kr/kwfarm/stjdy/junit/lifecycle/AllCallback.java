package kr.kwfarm.stjdy.junit.lifecycle;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AllCallback implements BeforeAllCallback, AfterAllCallback {
    private Logger logger = LoggerFactory.getLogger(AllCallback.class);

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        logger.info("10. AfterCallback");
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        logger.info("1. BeforeCallback");
    }
}
