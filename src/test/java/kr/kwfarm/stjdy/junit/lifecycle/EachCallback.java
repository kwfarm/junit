package kr.kwfarm.stjdy.junit.lifecycle;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EachCallback implements BeforeEachCallback, AfterEachCallback {
    private Logger logger = LoggerFactory.getLogger(EachCallback.class);

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        logger.info("8. afterEachCallback");
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        logger.info("3. beforeEachCallback");
    }
}
