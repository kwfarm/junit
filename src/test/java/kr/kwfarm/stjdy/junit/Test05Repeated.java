package kr.kwfarm.stjdy.junit;

import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Test05Repeated {
    private Logger logger = LoggerFactory.getLogger(Test05Repeated.class);

    @BeforeEach
    void beforeEach(TestInfo testInfo, RepetitionInfo repetitionInfo) {
        String methodName = testInfo.getTestMethod().get().getName();
        int total = repetitionInfo.getTotalRepetitions();
        int current = repetitionInfo.getCurrentRepetition();

        logger.info("=====[Before {}, totla: {}, current: {}]=====", methodName, total, current);
    }

    @RepeatedTest(2)
    void repeatedTest() {
        logger.info("repeatedTest");
    }

    @RepeatedTest(value = 2, name = "{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("Hello")
    void repeatedTest2() {
        logger.info("repeatedTest");
    }

    @RepeatedTest(2)
    @DisplayName("Fail")
    void repeatedTestFail(RepetitionInfo repetitionInfo) {
        Assertions.assertEquals(1, repetitionInfo.getCurrentRepetition());
    }
}
