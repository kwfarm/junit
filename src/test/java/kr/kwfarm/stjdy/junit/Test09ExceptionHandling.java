package kr.kwfarm.stjdy.junit;

import kr.kwfarm.stjdy.junit.lifecycle.AllCallback;
import kr.kwfarm.stjdy.junit.lifecycle.EachCallback;
import kr.kwfarm.stjdy.junit.lifecycle.ExceptionHandler;
import kr.kwfarm.stjdy.junit.lifecycle.ExecutionCallback;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@ExtendWith(AllCallback.class)
@ExtendWith({EachCallback.class, ExecutionCallback.class})
@ExtendWith({ExceptionHandler.class})
public class Test09ExceptionHandling {
    private static Logger logger = LoggerFactory.getLogger(Test08Lifecycle.class);

    @BeforeAll
    public static void beforeAll() {
        logger.info("2. BeforeAll");
    }

    @AfterAll
    public static void afterAll() {
        logger.info("9. afterAll");
    }

    @BeforeEach
    void beforeEach() {
        logger.info("4. beforeEach");
    }

    @AfterEach
    void afterEach() {
        logger.info("7. afterEach");
    }

    @Test
    void test() throws IOException {
        logger.info("Test01");
        new Foo().exception();
    }

    static class Foo {
        void exception() throws IOException {
            throw new IOException();
        }
    }
}
