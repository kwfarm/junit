package kr.kwfarm.stjdy.junit;

import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Tag("foo")
public class Test04TestInfo {
    private Logger logger = LoggerFactory.getLogger(Test04TestInfo.class);

    public Test04TestInfo(TestInfo testInfo) {
        logger.info("{}", testInfo);
    }

    @BeforeEach
    void beforeEach(TestInfo testInfo) {
        logger.info("BeforeEach: {}", testInfo);
    }

    @Tag("test01")
    @Test
    void test01(TestInfo testInfo) {
        List<String> tags = new ArrayList<>();
        tags.addAll(testInfo.getTags());

        assertEquals(tags, Arrays.asList("test01", "foo"));
        assertEquals(testInfo.getTestMethod().get().getName(), "test01");
    }

    @Test
    void test02(TestInfo testInfo) {
        List<String> tags = new ArrayList<>();
        tags.addAll(testInfo.getTags());

        assertEquals(tags, Arrays.asList("foo"));
        assertEquals(testInfo.getTestMethod().get().getName(), "test02");
    }
}
