package kr.kwfarm.stjdy.junit;

import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

public class Test01Annotion {
    private static Logger logger = LoggerFactory.getLogger(Test01Annotion.class);

    @BeforeAll
    public static void beforeAll() {
        logger.info("==========[Only One Befor Run]==========");
    }

    @BeforeEach
    public void beforEach() {
        logger.info("=====[Each Befor Run]=====");
    }

    @Test
    public void first() {
        logger.info("First Test");
    }

    @Test
    @DisplayName("Hello~")
    public void second() {
        logger.info("Second Test");
    }

    @Test
    @Disabled("Not Working")
    public void third() {
        fail("Fail!!!!");
    }

    @Test
    @Disabled("Not Working2")
    @DisplayName("Hello2")
    public void fourth() {
        fail("Fail!!!!");
    }

    @AfterEach
    public void afterEach() {
        logger.info("=====[Each After Run]=====");
    }

    @AfterAll
    public static void afterAll() {
        logger.info("==========[Only One After Run]==========");
    }
}
